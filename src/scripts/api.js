const USER_ID = '8eb676f7-7664-4a78-bbfa-6d04c34bd102';
const BASE_URL = `https://glo3102lab4.herokuapp.com/${USER_ID}`;

export const createTodo = async (name) => {
    try {
        const response = await fetch(`${BASE_URL}/tasks`, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify({ 
                name: name
            })
        });
        const json = await response.json();
        return json;
    } catch (err) {
        alert("Cannot create todo : " + err.message);
    }
};

export const getTodos = async () => {
    try {
        const response = await fetch(`${BASE_URL}/tasks`);
        const json = await response.json();
        return json.tasks;
    } catch (err) {
        alert("Cannot delete todo : " + err.message);
    }
};

export const deleteTodo = async (id) => {
    try {
        return await fetch(`${BASE_URL}/tasks/${id}`, {
            method: 'DELETE'
        });
    } catch (err) {
        alert("Cannot delete todo : " + err.message);  
    }
};

export const updateTodo = async (taskId, newName) => {
    try {
        const response = await fetch(`${BASE_URL}/tasks/${taskId}`, {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify({ 
                name: newName
            })
        });
        const json = await response.json();
        return json;
    } catch (err) {
        alert("Cannot create todo : " + err.message);
    }
};